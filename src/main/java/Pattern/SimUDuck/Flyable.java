package Pattern.SimUDuck;

public interface Flyable {
    void fly();
}
