package Pattern.SimUDuck;

public class Duck {
    Flyable flyBehavior;
    Quackable quackBehavior;

    public void swim(){
        System.out.println("I am swimming");
    }

    public void display(){
        System.out.println("I am duck");
    }

    public void performFly(){
        flyBehavior.fly();
    }

    public void performQuack(){
        quackBehavior.quack();
    }

    public void setFlyBehavior(Flyable flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void setQuackBehavior(Quackable quackBehavior) {
        this.quackBehavior = quackBehavior;
    }
}
