package Pattern.SimUDuck;

public class ReadHeadDuck extends Duck{
    @Override
    public void display() {
        System.out.println("I am Read Head duck");
    }

    public ReadHeadDuck(){
        flyBehavior = new FlyWithWiings();
        quackBehavior = new Quack();
    }

}
