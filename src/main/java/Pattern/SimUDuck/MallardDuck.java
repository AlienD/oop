package Pattern.SimUDuck;

public class MallardDuck extends Duck {
    @Override
    public void display() {
        System.out.println("I am Mallard duck");
    }

    public MallardDuck(){
        flyBehavior = new FlyWithWiings();
        quackBehavior = new Quack();
    }
}
