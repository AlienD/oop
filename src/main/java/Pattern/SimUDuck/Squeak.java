package Pattern.SimUDuck;

public class Squeak implements Quackable {
    public void quack() {
        System.out.println("squeak squeak");
    }
}
