package Pattern.SimUDuck;

public class Main {
    public static void main(String[] args) {
        Duck simpleDuck1 = new Duck();
        MallardDuck mallardDuck1 = new MallardDuck();
        ReadHeadDuck readHeadDuck1 = new ReadHeadDuck();
        RubberDuck rubberDuck1 = new RubberDuck();

        simpleDuck1.display();
        mallardDuck1.display();
        readHeadDuck1.display();

        readHeadDuck1.performFly();
        readHeadDuck1.performQuack();

        rubberDuck1.performFly();
        rubberDuck1.performQuack();
    }
}
