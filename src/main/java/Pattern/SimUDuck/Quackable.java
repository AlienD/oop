package Pattern.SimUDuck;

public interface Quackable {
    void quack();
}
