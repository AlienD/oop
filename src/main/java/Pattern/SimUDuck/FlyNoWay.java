package Pattern.SimUDuck;

public class FlyNoWay implements Flyable {
    public void fly() {
        System.out.println("I cant fly");
    }
}
