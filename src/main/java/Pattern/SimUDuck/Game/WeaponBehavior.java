package Pattern.SimUDuck.Game;

public interface WeaponBehavior {
    void useWeapon();
}
