package Pattern.SimUDuck.Game;

public class SwordBehavior implements WeaponBehavior {
    public void useWeapon() {
        System.out.println("I use sword");
    }
}
