package Pattern.SimUDuck.Game;

public class BowAndArrowBehavior implements WeaponBehavior {
    public void useWeapon() {
        System.out.println("I use bow and arrow");
    }
}
