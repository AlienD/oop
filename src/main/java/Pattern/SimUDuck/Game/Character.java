package Pattern.SimUDuck.Game;

public class Character {
    WeaponBehavior weapon;

    public void fight(){
        System.out.println("I fight");
    }

    public void setWeapon(WeaponBehavior weapon) {
        this.weapon = weapon;
    }

    public void weaponForm(){
        weapon.useWeapon();
    }
}
