package Pattern.SimUDuck.Game;

public class KnifeBehavior implements WeaponBehavior {
    public void useWeapon() {
        System.out.println("I use knife");
    }
}
