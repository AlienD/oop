package Pattern.SimUDuck.Game;

public class AxeBehavior implements WeaponBehavior {
    public void useWeapon() {
        System.out.println("I use axe");
    }
}
