package Pattern.SimUDuck.Game;

public class Main {
    public static void main(String[] args) {
        Knight Alisher = new Knight();
        King Arman = new King();
        Queen Aruzhan = new Queen();
        Troll Troll = new Troll();

        Alisher.weaponForm();
        Aruzhan.weaponForm();
        Aruzhan.setWeapon(new KnifeBehavior());
        Aruzhan.weaponForm();
    }
}
