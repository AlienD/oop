package Pattern.SimUDuck;

public class FlyWithWiings implements Flyable {
    public void fly() {
        System.out.println("I can fly");
    }
}
