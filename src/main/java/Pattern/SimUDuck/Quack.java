package Pattern.SimUDuck;

public class Quack implements Quackable {
    public void quack() {
        System.out.println("quack quack");
    }
}
