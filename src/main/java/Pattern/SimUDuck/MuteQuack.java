package Pattern.SimUDuck;

public class MuteQuack implements Quackable {
    public void quack() {
        System.out.println("...");
    }
}
