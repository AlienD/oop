package Pattern.Nabludatel.TleuzhansExample;

import java.util.ArrayList;
import java.util.List;

public class Magazine implements Subject {
    private List<Observer> subscribers;
    private List<String> vacancies;

    public Magazine(){
        subscribers = new ArrayList<Observer>();
        vacancies = new ArrayList<String>();
    }

    public void addVacancy(String vacancy){
        vacancies.add(vacancy);
        notifyAllObservers();
    }

    public void removeVacancy(String vacancy){
        vacancies.remove(vacancy);
        notifyAllObservers();
    }

    public void addObserver(Observer observer) {
        subscribers.add(observer);
    }

    public void removeObserver(Observer observer) {
        subscribers.remove(observer);
    }

    public void notifyAllObservers() {
        for (Observer subscriber: subscribers){
            subscriber.handleEvent(vacancies);
        }
    }
}
