package Pattern.Nabludatel.TleuzhansExample;

import java.util.List;

public class Subscriber implements Observer{
    String name;

    public Subscriber(String name){
        this.name = name;
    }

    public void handleEvent(List<String> vacancies) {
        System.out.println("Dear "+name+" we have new changes ");
        for (String element: vacancies){
            System.out.println(element);
        }
    }
}
