package Pattern.Nabludatel.TleuzhansExample;

public class Main {
    public static void main(String[] args) {
        Magazine magazine1 = new Magazine();
        Subscriber Alisher = new Subscriber("Alisher");
        Subscriber Alikhan = new Subscriber("Alikhan");
        Subscriber Venera = new Subscriber("Venera");
        magazine1.addObserver(Alisher);
        magazine1.addVacancy("Junior JAVA developer");
        magazine1.addVacancy("Senior C++ developer");
        magazine1.addVacancy("Full Stack programmist");
        magazine1.removeObserver(Alisher);
        magazine1.addObserver(Alikhan);
        magazine1.addVacancy("PHP developer");

        magazine1.addObserver(Venera);



    }
}
