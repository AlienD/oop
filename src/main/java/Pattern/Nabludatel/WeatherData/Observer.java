package Pattern.Nabludatel.WeatherData;

public interface Observer {
    public void update(float temperature, float humidity, float pressure);
}
