package Pattern.Nabludatel.WeatherData;

public interface DisplayElement {
    public void display();
}
