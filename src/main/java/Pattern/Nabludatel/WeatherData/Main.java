package Pattern.Nabludatel.WeatherData;

public class Main {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();

        CurrentConditionDisplay currentConditionDisplay = new CurrentConditionDisplay(weatherData);
        CurrentConditionDisplay ciii = new CurrentConditionDisplay(weatherData);

        weatherData.setMeasurements(72,10,22);

        currentConditionDisplay.exitFrom(weatherData);

        weatherData.setMeasurements(71,45,84);

    }
}
