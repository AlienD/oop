package assignment4;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    public static void main(String[] args) {
        Warrior warrior = new Warrior();

        warrior.putOnArmor();
        warrior.putOnHelmet();
        warrior.takeHorse();
        try {
            warrior.goToGait();
        } catch (WarriorIsNotReady warriorIsNotReady) {
            try {
                PrintWriter outputStream = new PrintWriter(new FileWriter("characterOut.txt"));
                outputStream.println(warriorIsNotReady.getMessage());
                outputStream.println(warriorIsNotReady.getLocalizedMessage());
                outputStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            warriorIsNotReady.printStackTrace();
        }
    }
}
