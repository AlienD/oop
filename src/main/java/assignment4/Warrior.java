package assignment4;

import exception.DogIsNotReadyException;

public class Warrior {
    private boolean sword = false;
    private boolean shield = false;
    private boolean armor = false;
    private boolean horse = false;
    private boolean helmet = false;

    public void putOnArmor(){
        System.out.println("Доспех надет");
        this.armor = true;
    }

    public void putOnHelmet(){
        System.out.println("Шлем надет");
        this.helmet = true;
    }

    public void takeSword(){
        System.out.println("Мечь готов");
        this.sword = true;
    }

    public void takeShield(){
        System.out.println("Щит готов");
        this.shield = true;
    }

    public void takeHorse(){
        System.out.println("Лошадь готов к походке");
        this.horse = true;
    }

    public void goToGait() throws WarriorIsNotReady {
        if (shield && helmet && sword && horse && armor){
            System.out.println("Воин готов к походке");
        } else {
            throw new WarriorIsNotReady("Воин не готов к походке! Проверьте все экипировку!");
        }
    }
}
