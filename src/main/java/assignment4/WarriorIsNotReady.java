package assignment4;

public class WarriorIsNotReady extends Exception {
    public WarriorIsNotReady(String message){
        super(message);
    }
}
