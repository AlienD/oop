package io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Locale;
import java.util.Scanner;

public class ScannerIO {
    public static void main(String[] args) {
        Scanner s = null;
        double sum = 0;

        try {
            s = new Scanner(new BufferedReader(new FileReader("C:\\Users\\user\\Desktop\\cs1902.txt")));
            // s.useDelimiter(","); // который обьект является индексом показывающий что это next string
            s.useLocale(Locale.US); // для чтение чисел
            while (s.hasNext()){
                if (s.hasNextDouble()) {
                    sum += s.nextDouble();
                } else {
                    System.out.println(s.next());
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(sum);

        // https://docs.oracle.com/javase/tutorial/essential/io/formatting.html
    }
}
