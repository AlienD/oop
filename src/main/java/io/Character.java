package io;

import java.io.*;

public class Character {
    public static void main(String[] args) {
        /*
        FileReader inputStream = null;
        FileWriter outputStream = null;

        try {
            inputStream = new FileReader("C:\\Users\\user\\Desktop\\cs1902.txt");
            outputStream = new FileWriter("characterOut.txt");
            int c;
            while ((c=inputStream.read())!=-1){
                outputStream.write(c);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } previous version
        */
        BufferedReader inputStream = null;
        PrintWriter outputStream = null;

        try {
            inputStream = new BufferedReader(new FileReader("C:\\Users\\user\\Desktop\\cs1902.txt"));
            outputStream = new PrintWriter(new FileWriter("characterOut.txt"));
            String l;

            while ((l=inputStream.readLine())!=null) {
                System.out.println(l);
                outputStream.println(l);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
