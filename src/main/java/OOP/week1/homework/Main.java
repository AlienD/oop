package OOP.week1.homework;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("A",2001);
        Person person2 = new Person("B",2010);
        Person person3 = new Person("C",2009);
        Person person4 = new Person("D",1980);
        Person person5 = new Person("E",1840);
        System.out.println(person1.calculateAge());
    }
}
