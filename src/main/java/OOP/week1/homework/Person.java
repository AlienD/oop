package OOP.week1.homework;

import java.util.Calendar;

public class Person {
    private String name;
    private int birthYear;

    // constructor
    public Person(){}

    public Person(String name, int birthYear){
        this.name = name;
        this.birthYear = birthYear;
    }

    // getter setter
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    // methods
    public int calculateAge(){
        if (birthYear==0) {
            System.out.println("The birthYear is not setted");
            return 0;
        }
        int year = Calendar.getInstance().get(Calendar.YEAR);
        return year - birthYear;
    }

    public void inputAlldata(String name, int birthYear){
        if (this.name==null){
            this.name = name;
        } else{
            System.out.println("You cant set new name, because its already exists");
        }

        if (this.birthYear==0){
            this.birthYear = birthYear;
        } else {
            System.out.println("You cant set new birthYear, because its already exists");
        }
    }

    @Override
    public String toString(){
        return "Person [ name: " + name+", birth year: "+birthYear+"]";
    }

    public void changeName(String name){
        this.name = name;
    }
}
