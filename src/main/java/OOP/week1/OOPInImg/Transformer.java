package OOP.week1.OOPInImg;

public class Transformer {
    private int position;
    private Gun gunInLeftHand;
    private Gun gunInRightHand;

    // constructor
    public Transformer(int position){
        this.position = position;;
        gunInLeftHand = new Gun(18);
        gunInRightHand = new Gun(30);
    }

    // getter setter
    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    // method
    public void run(){
        position++;
    }

    public void fire(){
        gunInRightHand.fire();
        gunInLeftHand.fire();
    }
}
