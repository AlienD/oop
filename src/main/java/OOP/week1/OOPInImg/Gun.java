package OOP.week1.OOPInImg;

public class Gun {
    private int size;
    private int bulletsInMagazine;

    public Gun(int size){
        this.size = size;
        reload();
    }

    // getter setter
    public int getBulletsInMagazine() {
        return bulletsInMagazine;
    }

    public int getSize() {
        return size;
    }

    public void setBulletsInMagazine(int bulletsInMagazine) {
        this.bulletsInMagazine = bulletsInMagazine;
    }

    public void setSize(int size) {
        this.size = size;
    }

    // method
    public void fire(){
        bulletsInMagazine-=1;
    }

    public void reload(){
        bulletsInMagazine = size;
    }
}
