package OOP.week1.OOPInImg;

public class Main {
    public static void main(String[] args) {

        Transformer optimus = new Transformer(0);   // Main - client of transformer
        Transformer bumblebee = new Transformer(1);

        System.out.println(optimus.getPosition());
    }
}
