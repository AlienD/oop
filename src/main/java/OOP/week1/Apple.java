package OOP.week1;

public class Apple {
    String color;
    boolean isTasty;
    double weight;

    // Constructor
    public Apple(String color){
        this.color = color;
    }

    // Overloading
    public Apple(){
    }

}
