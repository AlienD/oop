package OOP.week1.example;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student();
        student1.setName("A");
        student1.setRating(8.9);
        Student student2 = new Student();
        student2.setName("B");
        student2.setRating(8.9);
        student1.changeRating(7.1);
        student2.changeRating(10.7);
        System.out.println(Student.getAvgRating());
        System.out.println(Student.getTotalRating());
    }
}
