package OOP.week1.practical;

public class Main {
    public static void main(String[] args) {
        Employee employee1 = new Employee("A",8.3,10);
        Employee employee2 = new Employee("A",8.3,10);
        Employee employee3 = new Employee("A",8.3,10);
        employee1.setHours(11);
        System.out.println(employee1.knowSalary());
        System.out.println(employee1);
        System.out.println(employee1.bonuses());
        System.out.println(Employee.getTotalSum());

        System.out.println(Employee.getTotalHour());
        System.out.println(Employee.getTotalSum());
    }
}
