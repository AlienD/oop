package OOP.week3;

public class Circle extends Figure{
    int radius = 10;

    @Override
    void area() {
        System.out.println("Area of circle: "+radius*radius*3.14);
    }

    @Override
    void display() {
        System.out.println("drawing circle");
    }
}
