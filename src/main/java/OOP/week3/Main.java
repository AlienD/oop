package OOP.week3;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Figure shape1 = new Circle();
        Figure shape2 = new Square();

        List<Figure> listFigure = new ArrayList<>();
        listFigure.add(shape1);
        listFigure.add(shape2);

        for (Figure shape: listFigure){
            shape.display();
        }


    }
}
