package OOP.week3;

public abstract class Figure {
    int numberOfSides;

    abstract void area();
    abstract void display();
}
