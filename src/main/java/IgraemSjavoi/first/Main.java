package IgraemSjavoi.first;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        Reader reader = new Reader();
        reader.readText();
        String newText = reader.returnMyText();
        Writer writer = new Writer();
        writer.write(newText);
    }
}
