package IgraemSjavoi.first;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Reader {
    String myText = "";
    char lf = 0x0A ;
    String endLine = ""+lf;

    public void readText(){
        try (FileInputStream myFile = new FileInputStream("C:\\Users\\user\\Desktop\\cs1902.txt");
             InputStreamReader inputStreamReader = new InputStreamReader(myFile, "UTF-8");
             BufferedReader reader = new BufferedReader(inputStreamReader)) {
            String nextLine;
            boolean eof = false;
            while (!eof) {
                nextLine = reader.readLine();
                if (nextLine == null){
                    eof = true;
                } else {
                    myText += nextLine + endLine;
                    // myTextArray.add(nextLine);
                }
            }
        }catch (IOException e){
            System.out.println("Can't read Stalking.txt");
        }
    }

    public String returnMyText(){
        return myText;
    }
}
