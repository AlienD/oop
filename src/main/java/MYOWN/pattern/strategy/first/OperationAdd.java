package MYOWN.pattern.strategy.first;

public class OperationAdd implements OperationType{
    @Override
    public float neededValues(float value1, float value2) {
        return value1+value2;
    }
}
