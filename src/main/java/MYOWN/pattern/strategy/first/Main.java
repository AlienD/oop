package MYOWN.pattern.strategy.first;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Context context = new Context();
        int operation;
        float value1;
        float value2;
        System.out.println("Первое число: ");
        value1 = scanner.nextFloat();
        System.out.println("Второе число: ");
        value2 = scanner.nextFloat();
        do {
            System.out.println("Выбирайте операцию: 0-end, 1-addition, 2-subtraction, 3-multiplication");
            operation = scanner.nextInt();

            switch (operation) {
                case 1:
                    context.setOperationType(new OperationAdd());
                    break;
                case 2:
                    context.setOperationType(new OperationSubtract());
                    break;
                case 3:
                    context.setOperationType(new OperationMultiply());
                    break;
                default: continue;
            }
            System.out.println(context.getOperationType().neededValues(value1, value2));
        } while (operation != 0);


    }
}
