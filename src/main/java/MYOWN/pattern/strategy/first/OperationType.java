package MYOWN.pattern.strategy.first;

public interface OperationType {
    public float neededValues(float value1,float value2);
}
