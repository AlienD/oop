package MYOWN.pattern.strategy.first;

public class Context {
    private OperationType operationType;

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public OperationType getOperationType() {
        return operationType;
    }
}
