package MYOWN.pattern.decorator;

public class Decaf extends Beverage {
    public Decaf(){
        setDescription("Decaf Coffee");
    }
    @Override
    public double cost() {
        return 0.6;
    }
}
