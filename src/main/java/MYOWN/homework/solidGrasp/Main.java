package MYOWN.homework.solidGrasp;

public class Main {
    public static void main(String[] args) {
        Good tShirt = new Good("tShirt", 2500);
        Good shirt = new Good("shirt", 4500);
        Good jeans = new Good("jeans", 6000);

        OrderItem orderItem1 = new OrderItem(tShirt);
        orderItem1.addAmount();
        orderItem1.addAmount();
        OrderItem orderItem2 = new OrderItem(shirt, 4);
        OrderItem orderItem3 = new OrderItem(jeans, 2);

        Order order = new Order();
        order.addOrderItem(orderItem1);
        order.addOrderItem(orderItem2);

        System.out.println(order.calculateOrder());
    }
}
