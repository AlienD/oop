package MYOWN.homework.solidGrasp;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private List<OrderItem> list;

    public Order(){
        list = new ArrayList<OrderItem>();
    }

    public void addOrderItem(OrderItem orderItem){
        list.add(orderItem);
    }

    public List<OrderItem> getList() {
        return list;
    }

    public double calculateOrder(){
        float sum = 0;
        for (OrderItem order: list) {
            sum+=order.getPriceOfOrderItem();
        }
        return sum;
    }
}
