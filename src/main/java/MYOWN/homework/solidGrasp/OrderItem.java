package MYOWN.homework.solidGrasp;

import java.util.ArrayList;
import java.util.List;

public class OrderItem {
    private Good good;
    private int amount = 0;

    public OrderItem(Good good){
        this.good = good;
        this.amount = 1;
    }

    public OrderItem(Good good, int amount){
        this.good = good;
        this.amount = amount;
    }

    public void addAmount(){
        this.amount++;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Good getGood() {
        return good;
    }

    public double getPriceOfOrderItem(){
        return this.getGood().getPrice() * this.getAmount();
    }
}
