package MYOWN.homework.factory;

public class EnglishStrawberry extends Cake {
    public EnglishStrawberry(){
        setName("English Strawberry Cake");
        setCost(2500);

        components.add("Sugar Strawberry Eggs");
    }
}
