package MYOWN.homework.factory;

public class EnglishLemon extends Cake {
    public EnglishLemon(){
        setName("English Lemon Cake");
        setCost(2500);

        components.add("Sugar Lemon Eggs");
    }
}
