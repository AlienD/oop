package MYOWN.homework.factory;

public class AmericanCakeStore extends CakeStore {
    @Override
    Cake createCake(String type) {
        if (type.equals("strawberry")){
            return new AmericanStrawberry();
        } else if (type.equals("apple")){
            return new AmericanApple();
        } else {
            return null;
        }
    }
}
