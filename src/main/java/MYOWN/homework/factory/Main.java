package MYOWN.homework.factory;

public class Main {
    public static void main(String[] args) {
        CakeStore englishStore = new EnglishCake();
        CakeStore americanStore = new AmericanCakeStore();

        Cake cake = americanStore.orderCake("strawberry");
        System.out.println("Alisher ordered a "+ cake.getName());
        cake = englishStore.orderCake("lemon");
        System.out.println("Alisher ordered a "+cake.getName());
    }
}
