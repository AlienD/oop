package MYOWN.homework.factory;

public class AmericanStrawberry extends Cake {
    public AmericanStrawberry(){
        setName("American Strawberry Cake");
        setCost(1500);

        components.add("Sugar Strawberry Eggs");
    }
}
