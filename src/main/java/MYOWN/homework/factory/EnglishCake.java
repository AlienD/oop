package MYOWN.homework.factory;

public class EnglishCake extends CakeStore {

    @Override
    Cake createCake(String type) {
        if (type.equals("strawberry")){
            return new EnglishStrawberry();
        } else if (type.equals("lemon")){
            return new EnglishLemon();
        } else {
            return null;
        }
    }
}
