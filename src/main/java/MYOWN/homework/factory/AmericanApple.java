package MYOWN.homework.factory;

public class AmericanApple extends Cake {
    public AmericanApple(){
        setName("American Apple Cake");
        setCost(1200);

        components.add("Eggs Apple Sugar");
    }
}
