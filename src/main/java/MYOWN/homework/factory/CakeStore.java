package MYOWN.homework.factory;

public abstract class CakeStore {
    public Cake orderCake(String type){
        Cake cake;

        cake = createCake(type);

        cake.prepare();
        cake.bake();
        cake.cut();

        return cake;
    }

    abstract Cake createCake(String type);

}
