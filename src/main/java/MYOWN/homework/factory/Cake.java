package MYOWN.homework.factory;

import java.util.ArrayList;

public abstract class Cake {
    private String name;
    private double cost;
    ArrayList<String> components = new ArrayList<String>();

    void prepare(){
        System.out.println("Preparing " + name);
        System.out.println("Adding components: ");
        for (String component : components) {
            System.out.println(" " + component);
        }
    }

    void bake(){
        System.out.println("Baking for 10 minutes at 350");
    }

    void cut(){
        System.out.println("Cutting cake into parts");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
