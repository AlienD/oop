package MYOWN.homework.liskov.solution;

public class VideoMediaPlayer extends MediaPlayer {
    public void playVideo() {
        System.out.println("Playing video...");
    }
}
