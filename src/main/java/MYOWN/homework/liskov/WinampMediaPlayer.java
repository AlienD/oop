package MYOWN.homework.liskov;

public class WinampMediaPlayer extends MediaPlayer {
    public void playVideo() {
        throw new VideoUnsupportedException();
    }
}
