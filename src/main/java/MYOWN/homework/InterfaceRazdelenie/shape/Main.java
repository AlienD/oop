package MYOWN.homework.InterfaceRazdelenie.shape;

public class Main {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10, 5);
        Rectangle rect2 = new Rectangle(20, 37);

        AreaCalculator ac = new AreaCalculator();

        System.out.println(ac.areaCalc(rect1, rect2));

        System.out.println("====================================================");

        Circle c1 = new Circle(20);
        Circle c2 = new Circle(17.2);
        Circle c3 = new Circle(9);

        AreaCalculator_v2 ac2 = new AreaCalculator_v2();
        System.out.println(ac2.areaCalc(c1,c2,c3));
    }
}
