package MYOWN.homework.InterfaceRazdelenie.shape;

public class AreaCalculator {
    public double areaCalc(Rectangle... rectangles){
        double area = 0;
        for (Rectangle rectangle: rectangles) {
            area += rectangle.getWidth() * rectangle.getHeight();
        }

        return area;
    }
}
